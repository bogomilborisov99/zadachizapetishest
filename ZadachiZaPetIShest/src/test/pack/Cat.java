package test.pack;

public class Cat extends Animal {

	public Cat() {
		super("Cat", "mice");
	}

	@Override
	protected String explainSelf() {
		return "Meow";
	}

}
