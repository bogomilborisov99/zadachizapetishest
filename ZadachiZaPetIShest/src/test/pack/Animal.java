package test.pack;

public abstract class Animal {

	public Animal(String name, String favouriteFood) {
		this.name = name;
		this.favouriteFood = favouriteFood;
	}

	private String name;
	private String favouriteFood;

	public String getName() {
		return name;
	}

	public String getFavouriteFood() {
		return favouriteFood;
	}

	protected abstract String explainSelf();

}
