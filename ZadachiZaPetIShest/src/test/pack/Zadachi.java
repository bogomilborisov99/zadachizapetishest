package test.pack;

public class Zadachi {

	public static void main(String[] args) {
		// zadachi za 5
		int testNumber = 153;
		System.out.println("1. Check for prime number");
		System.out.println(checkForPrimeNr(testNumber) ? testNumber + " is a prime number"
				: testNumber + " is not a prime number");
		System.out.println("2. Check for Armstrong number");
		System.out.println(checkForArmstrongNr(testNumber) ? testNumber + " is Armstrong number"
				: testNumber + " isn't Armstrong number");

		// zadachi za 6
		System.out.println("1. Remove duplicate records");
		String[] arr = { "aa", "bb", "aa", "cc", "dd", "bb", "ee", "ee", "aa", "cc", "dd" };
		System.out.print("Array before:");
		for (Object i : arr) {
			System.out.print(" " + i);
		}
		System.out.println();
		removeDuplicateRecs(arr);
		System.out.print("Reduced array:");
		for (Object i : arr) {
			System.out.print(" " + i);
		}
		System.out.println();

		// za vtorata zadacha vijte class Animal, Cat, Dog
		Dog dog = new Dog();
		System.out.println(dog.getName() + " says " + dog.explainSelf() + " and eats " + dog.getFavouriteFood());
		Cat cat = new Cat();
		System.out.println(cat.getName() + " says " + cat.explainSelf() + " and eats " + cat.getFavouriteFood());
	}

	// 1. Write a Java program to check if a given number is prime or not. Remember,
	// a prime number is a number which is not divisible by any other number, e.g.
	// 3, 5, 7, 11, 13, 17, etc. Be prepared for cross, e.g. checking till the
	// square root of a number, etc.
	private static boolean checkForPrimeNr(int number) {
		boolean primeNr = true;
		for (int i = 2; i <= number / 2; ++i) {
			if (number % i == 0) {
				primeNr = false;
				break;
			}
		}
		return primeNr;
	}

	// 3. A number is called an Armstrong number if it is equal to the cube of its
	// every digit. For example, 153 is an Armstrong number because of 153= 1+
	// 125+27, which is equal to 1^3+5^3+3^3. You need to write a program to check
	// if the given number is Armstrong number or not.
	private static boolean checkForArmstrongNr(int number) {
		int tempNumber = number;
		int sumOfDigits = 0;
		while (tempNumber > 0) {
			sumOfDigits += Math.pow((tempNumber % 10), 3);
			tempNumber = tempNumber / 10;
		}
		return number == sumOfDigits ? true : false;
	}

	// 1. Write a program to remove duplicates from an array in Java without using
	// the Java Collection API. The array can be an array of String, Integer or
	// Character.
	private static void removeDuplicateRecs(Object[] arr) {
		if (!(arr instanceof String[] || arr instanceof Integer[] || arr instanceof Character[])) {
			System.out.println("Wrong array type!");
			return;
		}

		Object[] tempArr = new Object[arr.length];
		int tempPos = 0;
		for (Object obj : arr) {
			if (checkRecExists(tempArr, tempPos, obj)) {
				continue;
			}
			for (int i = 0, j = arr.length - 1; i < arr.length / 2 && j >= arr.length / 2; i++, j--) {
				if (arr[i] == obj) {
					tempArr[tempPos] = arr[i];
					tempPos++;
					break;
				}
				if (arr[j] == obj) {
					tempArr[tempPos] = arr[j];
					tempPos++;
					break;
				}
			}
		}
		for (int i = 0; i < arr.length; i++) {
			arr[i] = tempArr[i];
		}
	}

	private static boolean checkRecExists(Object[] tempArr, Integer tempPos, Object rec) {
		if (tempPos == 0) {
			return false;
		}
		for (int i = 0, j = tempArr.length - 1; i < tempArr.length / 2 && j >= tempArr.length / 2; i++, j--) {
			if (tempArr[i] == rec) {
				return true;
			}
			if (tempArr[j] == rec) {
				return true;
			}
		}
		return false;
	}
}
